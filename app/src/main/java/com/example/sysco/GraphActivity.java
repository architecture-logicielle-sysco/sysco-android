package com.example.sysco;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.Volley;
import com.jjoe64.graphview.GraphView;
import com.jjoe64.graphview.LegendRenderer;
import com.jjoe64.graphview.helper.DateAsXAxisLabelFormatter;
import com.jjoe64.graphview.series.DataPoint;
import com.jjoe64.graphview.series.LineGraphSeries;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;


public class GraphActivity extends AppCompatActivity {

    GraphView graph;
    private Context mContext;
    private Spinner spinner;
    private Spinner spinner2;
    private final String ipAdress = "193.48.57.164";
    private final String port = "8787";


    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_graph);

        spinner = findViewById(R.id.spinner1);
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this,R.array.Numero_capteur,android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);

        spinner2 = findViewById(R.id.spinner3);
        ArrayAdapter<CharSequence> adapter2 = ArrayAdapter.createFromResource(this,R.array.Nom_data,android.R.layout.simple_spinner_item);
        adapter2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner2.setAdapter(adapter2);

        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

              @Override
              public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                  //GraphView graph = findViewById(R.id.temp_graph);

                  mContext = getApplicationContext();
                  String url = getUrl(position, spinner2.getItemAtPosition(spinner2.getSelectedItemPosition()).toString());

                  getDataFromServeur(url);
              }

              @Override
               public void onNothingSelected(AdapterView<?> parent) {

              }
            }
        );



        spinner2.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                //GraphView graph = findViewById(R.id.temp_graph);
                String text = parent.getItemAtPosition(position).toString();
                mContext = getApplicationContext();

                String url=getUrl(spinner.getSelectedItemPosition(),text);
                getDataFromServeur(url);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });
    }



    void getDataFromServeur(String urlFinal){

        // Initialize a new RequestQueue instance
        RequestQueue requestQueue = Volley.newRequestQueue(mContext);

        // Initialize a new JsonObjectRequest instance
        JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(
            Request.Method.GET,
            urlFinal,
            null,
            new Response.Listener<JSONArray>() {
                @Override
                public void onResponse(JSONArray response) {

                traitementReponseServeur(response);
                }
            },
            new Response.ErrorListener(){
                @Override
                public void onErrorResponse(VolleyError error){
                // Do something when error occurred
                Toast.makeText(getApplicationContext(),"Requête données impossible\n Vérifiez votre connection",Toast.LENGTH_SHORT).show();
                }
            }
        );

        // Add JsonObjectRequest to the RequestQueue
        requestQueue.add(jsonArrayRequest);
    }

    void configGraph(int minY, int maxY, int maX){

        graph.getViewport().setMinY(minY);
        graph.getViewport().setMaxY(maxY);
        graph.getViewport().setMaxX(maX);
        // ;
        //graph.getLegendRenderer().setVisible(false);
        graph.getViewport().setYAxisBoundsManual(true);
        //graph.getViewport().setXAxisBoundsManual(true);
        //graph.getGridLabelRenderer().setLabelFormatter(new DateAsXAxisLabelFormatter(getActivity()));
        //graph.getGridLabelRenderer().setNumHorizontalLabels(3); // only 4 because of the space
        //graph.getGridLabelRenderer().setNumHorizontalLabels(10);
        graph.getViewport().setScalableY(true);
        graph.getGridLabelRenderer().setHumanRounding(false);
        //graph.getLegendRenderer().setAlign(LegendRenderer.LegendAlign.TOP);
    }

    void traitementReponseServeur(JSONArray response) {

        graph = findViewById(R.id.temp_graph);

        LineGraphSeries<DataPoint> series= new LineGraphSeries<>();
        graph.removeAllSeries();

        double measure_value;
        double x=0;

        for (int i = 0; i < response.length(); i++) {
            try {

                JSONObject roomJson = response.getJSONObject(i);

                measure_value = roomJson.getInt("measure_value");
                String measure_name = roomJson.getString("measure_name");

                if (measure_name.equals("Oxygene")) {
                    series.appendData(new DataPoint(x,measure_value),false,response.length());
                    x=x+1;
                    configGraph(0,40,response.length());
                }
                else if (measure_name.equals("Dioxyde de carbone")) {
                    series.appendData(new DataPoint(x,measure_value),false,response.length());
                    x=x+1;
                    configGraph(0,5,response.length());
                }
                else if (measure_name.equals("Humidite")) {
                    series.appendData(new DataPoint(x,measure_value),false,response.length());
                    x=x+1;
                    configGraph(0,100,response.length());
                }
                else if (measure_name.equals("Temperature")) {
                    series.appendData(new DataPoint(x,measure_value),false,response.length());
                    x=x+1;
                    configGraph(0,50,response.length());
                }
                else if (measure_name.equals("Pression atmospherique")) {
                    series.appendData(new DataPoint(x,measure_value),false,response.length());
                    x=x+1;
                    configGraph(400,1500,response.length());
                }
                else if (measure_name.equals("Particules fines")) {
                    series.appendData(new DataPoint(x,measure_value),false,response.length());
                    x=x+1;
                    configGraph(0,50,response.length());
                }
                else if (measure_name.equals("Monoxyde de carbone")) {
                    series.appendData(new DataPoint(x,measure_value),false,response.length());
                    x=x+1;
                    configGraph(0,3,response.length());
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        graph.addSeries(series);
    }

    String getUrl(int pos,String text){
        String url= "http://" + ipAdress + ":" + port;

        if (text.equals("Oxygene")){
            url = url + "/api-datas/historic/room"+pos+"/Oxygene";
        }
        else if (text.equals("Dioxyde de carbone")){
            url = url + "/api-datas/historic/room"+pos+"/Dioxyde%20de%20carbone";
        }
        else if (text.equals("Humidite")){
            url = url + "/api-datas/historic/room"+pos+"/Humidite";

        }
        else if (text.equals("Temperature")){
            url = url + "/api-datas/historic/room"+pos+"/Temperature";
        }
        else if (text.equals("Pression atmospherique")){
            url = url + "/api-datas/historic/room"+pos+"/Pression%20atmospherique";
        }
        else if (text.equals("Particules fines")){
            url = url + "/api-datas/historic/room"+pos+"/Particules%20fines";
        }
        else if (text.equals("Monoxyde de carbone")){
            url = url + "/api-datas/historic/room"+pos+"/Monoxyde%20de%20carbone";
        }
        return url;
    }
}
