package com.example.sysco;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
import java.util.Timer;
import java.util.TimerTask;

import android.os.Handler;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;


public class RealTimeActivity extends AppCompatActivity implements AdapterView.OnItemSelectedListener {

    private Button graph;
    private TextView Ox_info;
    private TextView CO_info;
    private TextView CO2_info;
    private TextView temp_Info;
    private TextView humi_Info;
    private TextView PA_Info;
    private TextView PF_Info;
    private Spinner spinner;

    private final String ipAdress ="193.48.57.164";
    private final String port ="8787";

    Timer timer;
    TimerTask timerTask;

    final Handler handler = new Handler();

    private Context mContext;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second);

        graph = findViewById(R.id.button_graph);
        Ox_info = findViewById(R.id.tv_Ox);
        CO_info = findViewById(R.id.tv_CO);
        CO2_info = findViewById(R.id.tv_CO2);
        temp_Info = findViewById(R.id.tv_temp);
        humi_Info = findViewById(R.id.tv_humi);
        PA_Info = findViewById(R.id.tv_PA);
        PF_Info = findViewById(R.id.tv_PF);
        mContext = getApplicationContext();

        graph.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            Intent intent = new Intent(RealTimeActivity.this, GraphActivity.class);
            startActivity(intent);
            }
        });

        spinner = findViewById(R.id.spinner1);
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this,R.array.Numero_capteur,android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);
        spinner.setOnItemSelectedListener(this);
        startTimer();
    }


    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        String text = parent.getItemAtPosition(position).toString();
        getDataSalle(position);
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {
    }

    public void startTimer() {
        //set a new Timer
        timer = new Timer();
        initializeTimerTask();
        timer.schedule(timerTask, 5000, 20000); //
    }

    public void initializeTimerTask() {

        timerTask = new TimerTask() {
            public void run() {
            //use a handler to run a toast that shows the current timestamp
            handler.post(new Runnable() {
                public void run() {
                getDataSalle(spinner.getSelectedItemPosition());
                }
            });
            }
        };
    }

    void getDataSalle(int num_salle){
        String urlFinal = "http://" + ipAdress + ":" + port +"/api-datas/realtime/room"+num_salle;
        getDataFromServeur(urlFinal);
    }

    void getDataFromServeur(String urlFinal){

        // Initialize a new RequestQueue instance
        RequestQueue requestQueue = Volley.newRequestQueue(mContext);

        // Initialize a new JsonObjectRequest instance
        JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(
            Request.Method.GET,
            urlFinal,
            null,
            new Response.Listener<JSONArray>() {
                @Override
                public void onResponse(JSONArray response) {
                    traitementReponseServeur(response);
                }
            },
            new Response.ErrorListener(){
                @Override
                public void onErrorResponse(VolleyError error){
                // Do something when error occurred
                Toast.makeText(getApplicationContext(), error.toString() /*"Requête données impossible\nVérifiez votre connection"*/,Toast.LENGTH_SHORT).show();
                }
            }
        );
        // Add JsonObjectRequest to the RequestQueue
        requestQueue.add(jsonArrayRequest);
    }


    void traitementReponseServeur(JSONArray response) {

        for (int i = 0; i < response.length(); i++) {
            try {
                JSONObject roomJson = null;
                roomJson = response.getJSONObject(i);
                String measure_value = roomJson.getString("measure_value");
                String measure_name = roomJson.getString("measure_name");
                if (measure_name.equals("Dioxyde de carbone")) {
                    CO2_info.setText(measure_value + "%");
                } else if (measure_name.equals("Humidite")) {
                    humi_Info.setText(measure_value + " %");
                } else if (measure_name.equals("Monoxyde de carbone")) {
                    CO_info.setText(measure_value + " %");
                } else if (measure_name.equals("Oxygene")) {
                    Ox_info.setText(measure_value + " %");
                } else if (measure_name.equals("Temperature")) {
                    temp_Info.setText(measure_value + " °C");
                } else if (measure_name.equals("Pression atmospherique")) {
                    PA_Info.setText(measure_value + " hPa");
                } else if (measure_name.equals("Particules fines")) {
                    PF_Info.setText(measure_value + " kg/cm³");
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }
}


