package com.example.sysco;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.android.volley.RequestQueue;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;

import org.json.JSONException;
import org.json.JSONObject;


public class MainActivity extends AppCompatActivity {

    private EditText Name;
    private EditText Password;
    private TextView Info;
    private Button Login;
    private final String ipAdress = "193.48.57.164";
    private final String port = "8787";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Name = (EditText)findViewById(R.id.Id_User);
        Password = (EditText)findViewById(R.id.Id_psw);
        Info = (TextView)findViewById(R.id.tv_info);
        Login = (Button)findViewById(R.id.button);
        Info.setText("LOG IN");
        Login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            validate(Name.getText().toString(),Password.getText().toString());
            }
        });
    }

    private void validate(String userName,String userPassword)
    {
        if ((userName.equals("Admin") ) && (userPassword.equals("abc") ))                           //POUR FAIRE DES TEST SANS INTERNET
        {
            Info.setText("BAD PASSWORD" + userName + userPassword);
            Intent intent = new Intent(MainActivity.this, RealTimeActivity.class);
            startActivity(intent);

        }
        else {

            if(userName.isEmpty() || userPassword.isEmpty()) {
                Toast.makeText(getApplicationContext(), "Empty username/password not allowed.\n" +
                    "Please try again.", Toast.LENGTH_SHORT).show();
            }
            else{

                final RequestQueue queue = Volley.newRequestQueue(this);
                JSONObject obj = new JSONObject();

                try {
                    obj.put("username", userName);
                    obj.put("password", userPassword);

                    String url = "http://" + ipAdress + ":" + port + "/logTest";

                    JsonObjectRequest jsonObjectResquest = new JsonObjectRequest
                        (Request.Method.POST, url, obj, new Response.Listener<JSONObject>() {
                            @Override
                            public void onResponse(JSONObject response) {
                            //Toast.makeText(getApplicationContext(), response.toString(), Toast.LENGTH_SHORT).show();
                            Intent intent = new Intent(MainActivity.this, RealTimeActivity.class);
                            startActivity(intent);
                            }
                        }, new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                            Toast.makeText(getApplicationContext(),error.toString() /*"BAD USER OR PASSWORD"*/, Toast.LENGTH_SHORT).show();
                            }
                        });
                    queue.add(jsonObjectResquest);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
